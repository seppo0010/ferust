#[cfg(target_os="emscripten")]
extern crate libc;

#[cfg(target_os="emscripten")]
#[macro_use]
pub extern crate webplatform;

#[cfg(not(target_os="emscripten"))]
#[macro_use]
extern crate lazy_static;

#[cfg(target_os="emscripten")]
mod implementation {
    pub use webplatform::{init, spin, alert, HtmlNode};

    pub fn add_child(parent: i32, child: i32) {
        js! { (parent, child) b"\
            WEBPLATFORM.rs_refs[$0].appendChild(WEBPLATFORM.rs_refs[$1]);\
        \0" };
    }

    pub fn add_to_body(node: i32) {
        js! { (node) b"\
            document.getElementsByTagName('body')[0].appendChild(WEBPLATFORM.rs_refs[$0]);\
        \0" };
    }

    pub fn create_tag(tag_name: &str) -> i32 {
        js! { (tag_name) b"\
            var el = document.createElement(UTF8ToString($0));\
            return WEBPLATFORM.rs_refs.push(el) - 1;\
        \0" }
    }

    pub fn create_text_node(content: &str) -> i32 {
        js! { (content) b"\
            var el = document.createTextNode(UTF8ToString($0));\
            return WEBPLATFORM.rs_refs.push(el) - 1;\
        \0" }
    }
}

#[cfg(not(target_os="emscripten"))]
#[allow(unused_variables)]
mod implementation {
    use std::sync::RwLock;
    use std::collections::HashMap;
    use std::fmt::{Debug,Error,Formatter};

    lazy_static! {
        static ref NODES: RwLock<Vec<InternalNode<'static>>> = RwLock::new(Vec::new());
        static ref ROOT_NODES: RwLock<Vec<usize>> = RwLock::new(Vec::new());
        static ref DOCUMENT: webplatform::Document = webplatform::Document;
    }

    #[derive(Default)]
    struct Callbacks<'a> {
        inner: Vec<Box<Fn(webplatform::Event<'a>) + 'a>>
    }

    impl<'a> Callbacks<'a> {
        fn trigger(&self, node_id: i32) {
            for f in self.inner.iter() {
                f(webplatform::Event {
                    target: Some(webplatform::HtmlNode {
                        id: node_id,
                        doc: &DOCUMENT,
                    })
                });
            }
        }
    }

    unsafe impl<'a> Send for Callbacks<'a> {}
    unsafe impl<'a> Sync for Callbacks<'a> {}

    impl<'a> Debug for Callbacks<'a> {
        fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
            Ok(())
        }
    }

    #[derive(Debug)]
    enum InternalNode<'a> {
        Tag {
            tag_name: String,
            attributes: Vec<(String, String)>,
            id: i32,
            parent: Option<i32>,
            listeners: HashMap<String, Callbacks<'a>>,
        },
        Text {
            content: String,
            id: i32,
            parent: Option<i32>,
        }
    }

    impl<'a> InternalNode<'a> {
        fn format_attributes(attributes: &Vec<(String, String)>) -> String {
            attributes.iter().fold(String::new(), |agg, &(ref key, ref value)| {
                agg + " " + &key + "=\"" + &value.replace("\"", "\\\"") + "\""
            })
        }

        fn id(&self) -> i32 {
            match *self {
                InternalNode::Tag { id, .. } => id,
                InternalNode::Text { id, .. } => id,
            }
        }

        fn parent(&self) -> Option<i32> {
            match *self {
                InternalNode::Tag { ref parent, .. } => parent,
                InternalNode::Text { ref parent, .. } => parent,
            }.clone()
        }

        fn format_children(&self) -> String {
            NODES.read().unwrap().iter().filter(|node| {
                node.parent() == Some(self.id())
            }).fold(String::new(), |agg, node| agg + &node.to_string())
        }

        fn to_string(&self) -> String {
            match *self {
                InternalNode::Tag { ref tag_name, ref attributes, .. } => {
                    format!(
                        "<{tag_name}{attributes}>{children}</{tag_name}>",
                        tag_name=tag_name,
                        attributes=InternalNode::format_attributes(attributes),
                        children=self.format_children()
                    )
                },
                InternalNode::Text { ref content, .. } => content.clone(),
            }
        }
    }

    pub fn html() -> String {
        let nodes = NODES.read().unwrap();
        ROOT_NODES.read().unwrap().iter().fold(String::new(), |agg, node| {
            agg + &nodes[*node].to_string()
        })
    }

    pub mod webplatform {
        use implementation::{Callbacks,InternalNode,NODES};
        pub struct Document;

        impl Document {
            pub fn new() -> Document {
                Document
            }
        }

        pub struct HtmlNode<'a> {
            pub id: i32,
            pub doc: &'a Document,
        }

        pub struct Event<'a> {
            pub target: Option<HtmlNode<'a>>,
        }

        impl<'a> HtmlNode<'a> {
            pub fn on<F: Fn(Event) + 'static>(&self, s: &str, f: F) {
                let mut nodes = NODES.write().unwrap();
                let mut listeners = match nodes[self.id as usize] {
                    InternalNode::Tag { ref mut listeners, .. } => listeners,
                    _ => panic!("cannot add event listener in text node"),
                };
                listeners.entry(s.to_owned()).or_insert_with(|| Callbacks::default()).inner.push(Box::new(f));
            }

            pub fn trigger(&self, s: &str) {
                let nodes = NODES.read().unwrap();
                let listeners = match nodes[self.id as usize] {
                    InternalNode::Tag { ref listeners, .. } => listeners,
                    _ => panic!("cannot trigger event listener in text node"),
                };
                if let Some(ref l) = listeners.get(s) {
                    l.trigger(self.id);
                }
            }
        }
    }

    pub fn init() -> webplatform::Document {
        webplatform::Document::new()
    }

    pub fn spin() {}

    pub fn alert(txt: &str) {
        println!("{}", txt);
    }

    pub fn add_child(new_parent: i32, child: i32) {
        let mut nodes = NODES.write().unwrap();
        match nodes[child as usize] {
            InternalNode::Tag { ref mut parent, .. } => *parent = Some(new_parent),
            InternalNode::Text { ref mut parent, .. } => *parent = Some(new_parent),
        }
    }

    pub fn add_to_body(node: i32) {
        ROOT_NODES.write().unwrap().push(node as usize);
    }

    pub fn create_tag(tag_name: &str) -> i32 {
        let mut nodes = NODES.write().unwrap();
        let i = nodes.len() as i32;
        nodes.push(InternalNode::Tag {
            id: i,
            tag_name: tag_name.to_owned(),
            attributes: vec![],
            parent: None,
            listeners: HashMap::new(),
        });
        i
    }

    pub fn create_text_node(content: &str) -> i32 {
        let mut nodes = NODES.write().unwrap();
        let i = nodes.len() as i32;
        nodes.push(InternalNode::Text {
            id: i,
            content: content.to_owned(),
            parent: None,
        });
        i
    }

    #[cfg(test)]
    use std::ops::RangeFull;
    #[cfg(test)]
    use std::sync::Arc;

    #[cfg(test)]
    fn clean() {
        let mut me = NODES.write().unwrap();
        me.drain(RangeFull);
        let mut me = ROOT_NODES.write().unwrap();
        me.drain(RangeFull);
    }

    #[test]
    fn test_tag_nodes_body() {
        clean();
        let tag = create_tag("a");
        let text = create_text_node("click");
        add_child(tag, text);
        add_to_body(tag);
        assert_eq!(
            html(),
            "<a>click</a>"
        )
    }

    #[test]
    fn test_callbacks() {
        clean();
        let tag = create_tag("a");
        let text = create_text_node("click");
        add_child(tag, text);
        add_to_body(tag);
        let x = Arc::new(RwLock::new(1));
        let innerx = x.clone();
        webplatform::HtmlNode { id: tag, doc: &DOCUMENT }.on("click", move |_e| {
            let mut x = innerx.write().unwrap();
            *x = 2;
        });
        webplatform::HtmlNode { id: tag, doc: &DOCUMENT }.trigger("click");
        assert_eq!(*x.read().unwrap(), 2);
    }
}

pub use implementation::*;

pub trait Renderable: Default {
    fn create_nodes() -> Vec<i32>;
    fn root_nodes() -> Vec<i32>;
    fn add_listeners(inner: std::rc::Rc<std::cell::RefCell<Self>>,
                     nodes: &[i32],
                     doc: &webplatform::Document);
}

pub struct Component<R: Renderable> {
    inner: std::rc::Rc<std::cell::RefCell<R>>,
    nodes: Vec<i32>,
}

impl<R: Renderable> Component<R> {
    pub fn new(doc: &webplatform::Document) -> Self {
        let c = Component {
            inner: std::rc::Rc::new(std::cell::RefCell::new(R::default())),
            nodes: R::create_nodes(),
        };
        R::add_listeners(c.inner.clone(), &c.nodes, doc);
        c
    }

    pub fn add_to_body(&self) {
        for node in R::root_nodes() {
            add_to_body(node);
        }
    }
}
