use syn;

use node::Node;
use parser::parse_nodes;

pub fn get_struct_nodes(input: &syn::DeriveInput) -> Vec<Node> {
    parse_nodes(get_struct_html(input).trim().as_bytes()).unwrap().1
}

fn get_struct_html(input: &syn::DeriveInput) -> &str {
    if let syn::Body::Struct(syn::VariantData::Struct(ref fields)) = input.body {
        let ref ferust_meta_item = fields.iter().find(|f| match f.ident {
            Some(ref i) => i == "html",
            _ => false,
        }).expect("Unable to find html attribute").attrs.iter().find(|attr| {
            attr.value.name() == "ferust"
        }).expect("Html must have a 'ferust' attribute").value;

        let html_meta_item = match *ferust_meta_item {
            syn::MetaItem::List(_, ref items) => items.iter().find(|i| {
                match **i {
                    syn::NestedMetaItem::MetaItem(syn::MetaItem::NameValue(ref n, _)) => n == "html",
                    _ => false,
                }
            }),
            _ => panic!("Expected 'ferust' attribute to be a list")
        }.expect("'ferust' must have an 'html' meta item");

        match *html_meta_item {
            syn::NestedMetaItem::MetaItem(syn::MetaItem::NameValue(_, ref v)) => match *v {
                syn::Lit::Str(ref s, _) => s,
                _ => panic!("ferust's html must be a string"),
            },
            _ => unreachable!(),
        }
    } else {
        panic!("Expected Ferust to derive a struct, got {:?} instead", input.body);
    }
}

#[test]
fn gets_html() {
    let input = r#"
    struct MyStruct {
        #[ferust(html="<b>hello</b> world")]
        html: [u8; 0],
    }
    "#;
    assert_eq!(
        get_struct_html(&syn::parse_derive_input(&input.to_string()).unwrap()),
        "<b>hello</b> world"
    );
}

pub fn create_source(nodes: &[Node]) -> String {
    format!("let r = vec![{}];{}r",
        nodes.iter().map(|x| x.to_create_source()).collect::<Vec<_>>().join(""),
        nodes.iter().fold((String::new(), 0), |(s, i), node| {
            let v = node.to_append_source_child(i);
            (s + &v.0, v.1 + 1)
        }).0,
    )
}

pub fn root_nodes(nodes: &[Node]) -> Vec<i32> {
    nodes.iter().fold((vec![], 0), |(mut indices, index), n| {
        indices.push(index); (indices, index + n.count_successors() + 1)
    }).0
}

pub fn add_listeners(nodes: &[Node]) -> String {
    nodes.iter().fold((String::new(), 0), |(s, i), n| {
        let v = n.to_add_listeners_source(i);
        (s + &v.0, v.1 + 1)
    }).0
}
