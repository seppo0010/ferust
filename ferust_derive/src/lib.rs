extern crate proc_macro;
extern crate quote;
extern crate syn;
#[macro_use]
extern crate nom;

mod html;
mod node;
mod parser;

use proc_macro::TokenStream;

#[proc_macro_derive(Ferust, attributes(ferust))]
pub fn derive_ferust(input: TokenStream) -> TokenStream {
    let input = syn::parse_derive_input(&input.to_string()).unwrap();
    let nodes = html::get_struct_nodes(&input);

    format!(r##"
        #[allow(unused_variables)]
        impl ::ferust::Renderable for {} {{
            fn create_nodes() -> Vec<i32> {{ {create_nodes} }}
            fn add_listeners(inner: std::rc::Rc<std::cell::RefCell<Self>>, nodes: &[i32], doc: &::ferust::webplatform::Document) {{ {add_listeners} }}
            fn root_nodes() -> Vec<i32> {{ vec![{root_nodes}] }}
        }}
        "##,
        name=input.ident,
        create_nodes=html::create_source(&nodes),
        add_listeners=html::add_listeners(&nodes),
        root_nodes=html::root_nodes(&nodes).into_iter().map(|x| format!("{}", x)).collect::<Vec<_>>().join(",")
    ).parse().unwrap()
}

#[test]
fn test_create_source() {
    let input = r##"
        <p><b>hello</b> <i>{! world !}</i></p><img /><a>h3ll0</a>
    "##;
    let nodes = parser::parse_nodes(input.trim().as_bytes()).unwrap().1;
    assert_eq!(
        syn::parse_token_trees(&html::create_source(&nodes)),
        syn::parse_token_trees(r##"
        let r = vec![
            ::ferust::create_tag("p"),
            ::ferust::create_tag("b"),
            ::ferust::create_text_node("hello"),
            ::ferust::create_tag("i"),
            ::ferust::create_text_node({ world }),
            ::ferust::create_tag("img"),
            ::ferust::create_tag("a"),
            ::ferust::create_text_node("h3ll0"),
        ];
        ::ferust::add_child(r[0], r[1]);
        ::ferust::add_child(r[1], r[2]);
        ::ferust::add_child(r[0], r[3]);
        ::ferust::add_child(r[3], r[4]);
        ::ferust::add_child(r[6], r[7]);
        r
        "##)
    )
}

#[test]
fn test_root_nodes() {
    let input = r##"
        <p><b>hello</b> <i>{! world !}</i></p><img /><a>h3ll0</a>
    "##;
    let nodes = parser::parse_nodes(input.trim().as_bytes()).unwrap().1;
    assert_eq!(
        html::root_nodes(&nodes),
        vec![0, 5, 6]
    )
}

#[test]
fn test_add_listeners() {
    let input = r##"
        <p onmouseover={!  |me, e| { alert(1) }!}>
        <a onclick={! |me, e| { alert(2) } !}>Hello</a>
        <a onclick={! |me, e| { alert(3) } !}>World</a>
        </p>
        <p>trollolololol</p>
        <p onmousedown={! |me, event| { alert(4) } !}>meh</p>
    "##;
    let nodes = parser::parse_nodes(input.trim().as_bytes()).unwrap().1;
    assert_eq!(
        syn::parse_token_trees(&html::add_listeners(&nodes)),
        syn::parse_token_trees(r##"
            let myinner = inner.clone();
            ferust::webplatform::HtmlNode {
                id: { let i = nodes[0]; i },
                doc: doc
            }.on("mouseover", move |e| {
                let me = myinner.borrow();
                 { alert(1) }
            });

            let myinner = inner.clone();
            ferust::webplatform::HtmlNode {
                id: { let i = nodes[1]; i },
                doc: doc
            }.on("click", move |e| {
                let me = myinner.borrow();
                 { alert(2) }
            });

            let myinner = inner.clone();
            ferust::webplatform::HtmlNode {
                id: { let i = nodes[3]; i },
                doc: doc
            }.on("click", move |e| {
                let me = myinner.borrow();
                 { alert(3) }
            });

            let myinner = inner.clone();
            ferust::webplatform::HtmlNode {
                id: { let i = nodes[8]; i },
                doc: doc
            }.on("mousedown", move |event| {
                let me = myinner.borrow();
                 { alert(4) }
            });
        "##)
    )
}
