use std::str;

use quote;
use quote::ToTokens;
use syn;

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    Literal(String),
    Generator(String),
}

impl Value {
    pub fn from_literal(s: &[u8]) -> Result<Value, str::Utf8Error> {
        Ok(Value::Literal(str::from_utf8(s)?.to_owned()))
    }

    pub fn from_generator(s: &[u8]) -> Result<Value, str::Utf8Error> {
        Ok(Value::Generator(str::from_utf8(s)?.to_owned()))
    }

    pub fn to_source(&self) -> String {
        match *self {
            Value::Literal(ref v) => format!("\"{}\"", v.replace("\"", "\\\"")),
            Value::Generator(ref g) => format!("{{ {} }}", g),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Node {
    Text(Value),
    Tag {
        tag_name: String,
        attrs: Vec<(String, Option<Value>)>,
        /// `children` is `None` if and only if using a self-closing tag
        children: Option<Vec<Node>>,
    }
}

impl Node {
    pub fn with_value(value: Value) -> Result<Node, ()> {
        Ok(Node::Text(value))
    }

    pub fn to_create_source(&self) -> String {
        match *self {
            Node::Text(ref v) => format!("::ferust::create_text_node({}), ", v.to_source()),
            Node::Tag { ref tag_name, ref children, .. } => {
                let create_children = children.as_ref().map(|n| n.iter().fold(String::new(), |agg, child| {
                    agg + &child.to_create_source()
                })).unwrap_or("".to_owned());
                format!("::ferust::create_tag(\"{}\"),{}", tag_name, create_children)
            },
        }
    }

    pub fn count_successors(&self) -> i32 {
        match *self {
            Node::Tag { ref children, .. } => {
                children.as_ref().map(|n| n.iter().fold(0, |c, child| {
                    c + child.count_successors() + 1
                })).unwrap_or(0)
            },
            _ => 0,
        }
    }

    pub fn to_append_source_child(&self, index: i32) -> (String, i32) {
        match *self {
            Node::Tag { ref children, .. } => {
                children.as_ref().map(|n| n.iter().fold((String::new(), index), |(agg, c), child| {
                    let s = child.to_append_source_child(c + 1 as i32);
                    (
                        agg +
                        &format!("::ferust::add_child(r[{}], r[{}]);", index, c + 1 as i32) +
                        &s.0,
                        s.1 as i32
                    )
                }))
            },
            _ => None,
        }.unwrap_or(("".to_owned(), index))
    }

    fn prepare_code(v: &Value) -> String {
        let source = v.to_source();
        /*
        FnDecl { inputs: [Captured(Ident(ByValue(Immutable), Ident("self"), None), Infer), Captured(Ident(ByValue(Immutable), Ident("e"), None), Infer)], output: Default, variadic: false },
        Expr { node: Block(Normal, Block { stmts: [Expr(Expr { node: Call(Expr { node: Path(None, Path { global: false, segments: [PathSegment { ident: Ident("alert"), parameters: AngleBracketed(AngleBracketedParameterData { lifetimes: [], types: [], bindings: [] }) }] }), attrs: [] }, [Expr { node: Lit(Int(1, Unsuffixed)), attrs: [] }]), attrs: [] })] }), attrs: [] }
        */
        let tts = syn::parse_expr(&source).expect("expression");
        let bloc = match tts.node {
            syn::ExprKind::Block(_, b) => b,
            x => panic!("expected block expression, got {:?}", x),
        };
        assert_eq!(bloc.stmts.len(), 1, "Expected only one statement");
        let ref node = match bloc.stmts[0] {
            syn::Stmt::Expr(ref e) => e,
            ref x => panic!("expected statement expression, got {:?}", x),
        }.node;
        let (decl, content) = match *node {
            syn::ExprKind::Closure(_, ref decl, ref content) => (decl, content),
            ref x => panic!("expected closure, got {:?}", x),
        };
        let params = decl.inputs.iter().map(|x| match *x {
            syn::FnArg::Captured(syn::Pat::Ident(_, ref name, _), _) => name.as_ref(),
            ref x => panic!("not supported parameter {:?}", x),
        }).collect::<Vec<_>>();
        assert_eq!(params.len(), 2);
        let mut tokens = quote::Tokens::new();
        content.to_tokens(&mut tokens);
        format!(r#"
        move |{evt}| {{
            let {me} = myinner.borrow();
            {code}
        }}
        "#,
        me=params[0],
        evt=params[1],
        code=tokens.as_str())
    }

    pub fn to_add_listeners_source(&self, index: i32) -> (String, i32) {
        match *self {
            Node::Tag { ref children, ref attrs, .. } => {
                let s = attrs.iter().fold(String::new(), |s, attr| {
                    if &attr.0[..2] == "on" {
                        s + &format!(r##"
                        let myinner = inner.clone();
                        ferust::webplatform::HtmlNode {{
                            id: {{ let i = nodes[{index}]; i }},
                            doc: doc
                        }}.on("{event}", {code});
                        "##,
                        index=index,
                        event=&attr.0[2..],
                        code=attr.1.as_ref().map(|x| Node::prepare_code(&x)).unwrap_or("".to_owned())
                        )
                    } else {
                        s
                    }
                });
                children.as_ref().map(|n| n.iter().fold((s, index), |(agg, c), child| {
                    let s = child.to_add_listeners_source(c + 1 as i32);
                    (agg + &s.0, s.1 as i32)
                }))
            },
            _ => None,
        }.unwrap_or(("".to_owned(), index))
    }
}
