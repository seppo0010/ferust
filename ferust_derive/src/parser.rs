use std::str;

use nom::{IResult, alphanumeric, is_alphanumeric};

use node::{Node, Value};

fn is_valid_tag(chr: u8) -> bool {
    is_alphanumeric(chr) || chr == '-' as u8
}
named!(tag_or_attr_name<&str>, map_res!(take_while_s!(is_valid_tag), str::from_utf8));

named!(attr_value_literal<Value>,
    delimited!(
        tag!("\""),
        map_res!(
            escaped!(call!(alphanumeric), '\\', is_a!("\"n\\")),
            Value::from_literal
        ),
        tag!("\"")
    )
);

named!(value_generator<Value>,
    delimited!(
        tag!("{!"),
        map_res!(
            take_until!("!}"),
            Value::from_generator
        ),
        tag!("!}")
    )
);

named!(attr_value<Value>, alt!(attr_value_literal | value_generator));

named!(parse_attribute<(String, Option<Value>)>, do_parse!(
    attr_name: map_res!(take_until_either!(" >/="), str::from_utf8) >>
    value: opt!(
        do_parse!(
            tag!("=") >>
            value: attr_value >>
            (value)
        )
    ) >>
    ((attr_name.to_owned(), value))
));

named!(parse_attributes<Vec<(String, Option<Value>)> >, ws!(many0!(parse_attribute)));

named!(parse_open_tag<(&str, Vec<(String, Option<Value>)>)>,
    ws!(do_parse!(
        tag!("<") >>
        t: tag_or_attr_name >>
        p: opt!(parse_attributes) >>
        tag!(">") >>
        ((t, p.unwrap_or(vec![])))
    ))
);
named_args!(parse_close_tag<'a>(tag_name: &str) <&'a [u8]>, delimited!(tag!("</"), tag!(tag_name), tag!(">")));

named!(parse_tag<Node>, do_parse!(
    tag_name_attrs: parse_open_tag >>
    children: parse_nodes >>
    call!(parse_close_tag, tag_name_attrs.0) >>
    (Node::Tag {
        tag_name: tag_name_attrs.0.to_owned(),
        attrs: tag_name_attrs.1,
        children: Some(children),
    })
));

named!(parse_selfclosing_tag<Node>, ws!(do_parse!(
    tag!("<") >>
    t: tag_or_attr_name >>
    p: opt!(parse_attributes) >>
    tag!("/>") >>
    (Node::Tag {
        tag_name: t.to_owned(),
        attrs: p.unwrap_or(vec![]),
        children: None,
    })
)));

named!(parse_text_literal_node<Node>, do_parse!(
    t: map_res!(take_until_either!("{<"), str::from_utf8) >>
    (Node::Text(Value::Literal(t.to_owned())))
));

named!(parse_text_generator_node<Node>, map_res!(value_generator, Node::with_value));

named!(parse_text_node<Node>, alt!(parse_text_generator_node | parse_text_literal_node));

named!(pub parse_node<Node>, alt!(parse_tag | parse_selfclosing_tag | parse_text_node));

named!(pub parse_nodes<Vec<Node>>, many0!(parse_node));

#[test]
fn test_open_tag() {
    assert_eq!(
        (parse_open_tag(b"<hello></hello>").unwrap().1).0,
        "hello"
    );
}

#[test]
fn test_parse_tag() {
    assert_eq!(
        parse_tag(b"<hello></hello>").unwrap().1,
        Node::Tag { tag_name: "hello".to_owned(), children: Some(vec![]), attrs: vec![], }
    );
}

#[test]
fn test_parse_tag_with_dash() {
    assert_eq!(
        parse_tag(b"<hello-world></hello-world>").unwrap().1,
        Node::Tag { tag_name: "hello-world".to_owned(), children: Some(vec![]), attrs: vec![], }
    );
}

#[test]
fn test_parse_tag_content() {
    assert_eq!(
        parse_tag(b"<hello><world></world></hello>").unwrap().1,
        Node::Tag {
            tag_name: "hello".to_owned(),
            children: Some(vec![
                Node::Tag {
                    tag_name: "world".to_owned(),
                    children: Some(vec![]),
                    attrs: vec![],
                }
            ]),
            attrs: vec![],
        }
    );
}

#[test]
fn test_parse_node_content_invalid() {
    parse_tag(b"<hello><world></othertag></hello>").unwrap_err();
}

#[test]
fn test_parse_selfclosing_tag() {
    assert_eq!(
        parse_selfclosing_tag(b"<hello />").unwrap().1,
        Node::Tag { tag_name: "hello".to_owned(), children: None, attrs: vec![], }
    );
}

#[test]
fn test_parse_attrs_empty() {
    assert_eq!(
        parse_attributes(b"").unwrap().1,
        vec![]
    );
}

#[test]
fn test_parse_attrs_noval() {
    assert_eq!(
        parse_attribute(b"hello ").unwrap().1,
        ("hello".to_owned(), None)
    );
}

#[test]
fn test_parse_attrs_litval() {
    assert_eq!(
        parse_attributes(b"hello=\"world\"").unwrap().1,
        vec![("hello".to_owned(), Some(Value::Literal("world".to_owned())))]
    );
}

#[test]
fn test_parse_attrs_many() {
    assert_eq!(
        parse_attributes(b"hello=\"world\" asd=\"qwe\"").unwrap().1,
        vec![
            ("hello".to_owned(), Some(Value::Literal("world".to_owned()))),
            ("asd".to_owned(), Some(Value::Literal("qwe".to_owned()))),
        ]
    );
}

#[test]
fn test_parse_tag_attrs_noval() {
    assert_eq!(
        parse_node(b"<hello world />").unwrap().1,
        Node::Tag { tag_name: "hello".to_owned(), children: None, attrs: vec![
            ("world".to_owned(), None)
        ], }
    );
}

#[test]
fn test_parse_tag_attrs_litval() {
    assert_eq!(
        parse_node(b"<a hello=\"world\" hello=\"test\"></a>").unwrap().1,
        Node::Tag {
            tag_name: "a".to_owned(),
            attrs: vec![
                ("hello".to_owned(), Some(Value::Literal("world".to_owned()))),
                ("hello".to_owned(), Some(Value::Literal("test".to_owned()))),
            ],
            children: Some(vec![])
        }
    );
}

#[test]
fn test_parse_attr_generator() {
    assert_eq!(
        parse_attributes(b"hello={! world !}").unwrap().1,
        vec![("hello".to_owned(), Some(Value::Generator(" world ".to_owned())))]
    );
}

#[test]
fn test_text_literal_node() {
    assert_eq!(
        parse_nodes(b"<b>world</b>").unwrap().1,
        vec![Node::Tag {
            tag_name: "b".to_owned(),
            attrs: vec![],
            children: Some(vec![Node::Text(Value::Literal("world".to_owned()))]),
        }]
    );
}

#[test]
fn test_text_generated_node() {
    assert_eq!(
        parse_nodes(b"<b>{! world !}</b>").unwrap().1,
        vec![Node::Tag {
            tag_name: "b".to_owned(),
            attrs: vec![],
            children: Some(vec![Node::Text(Value::Generator(" world ".to_owned()))]),
        }]
    );
}

#[test]
fn test_text_node_mixed() {
    assert_eq!(
        parse_nodes(b"<b>hello {! world !}</b>").unwrap().1,
        vec![Node::Tag {
            tag_name: "b".to_owned(),
            attrs: vec![],
            children: Some(vec![
                Node::Text(Value::Literal("hello ".to_owned())),
                Node::Text(Value::Generator(" world ".to_owned())),
            ]),
        }]
    );
}

#[test]
fn test_text_node_generator_multiple() {
    assert_eq!(
        parse_nodes(b"<b>{! hello !}{! world !}</b>").unwrap().1,
        vec![Node::Tag {
            tag_name: "b".to_owned(),
            attrs: vec![],
            children: Some(vec![
                Node::Text(Value::Generator(" hello ".to_owned())),
                Node::Text(Value::Generator(" world ".to_owned())),
            ]),
        }]
    );
}

#[test]
fn test_multiple_root_nodes() {
    assert_eq!(
        parse_nodes(b"<img />hello<img />").unwrap().1,
        vec![Node::Tag {
            tag_name: "img".to_owned(),
            attrs: vec![],
            children: None,
        },
        Node::Text(Value::Literal("hello".to_owned())),
        Node::Tag {
            tag_name: "img".to_owned(),
            attrs: vec![],
            children: None,
        }]
    );
}
