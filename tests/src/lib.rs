#![feature(proc_macro, attr_literals)]

#[macro_use]
extern crate ferust_derive;
extern crate ferust;

#[derive(Ferust, Default)]
struct MyStruct {
    content: String,

    #[ferust(html=r##"
    <p onclick={! |me, evt| {
        ferust::alert(&me.content);
    }
    !}>hello</p>
    "##)]
    html: [u8; 0],
}

#[test]
fn it_works() {
    let doc = ferust::init();
    let component = ferust::Component::<MyStruct>::new(&doc);
    component.add_to_body();
    ferust::spin();
}
